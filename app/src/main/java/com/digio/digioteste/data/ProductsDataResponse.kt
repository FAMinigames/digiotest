package com.digio.digioteste.data

import com.digio.digioteste.data.products.entities.CashEntity
import com.digio.digioteste.data.products.entities.ProductsEntity
import com.digio.digioteste.data.products.entities.SpotLightEntity
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class ProductsDataResponse : Serializable {
    @SerializedName("spotlight")
    var spotLightEntity: List<SpotLightEntity> = ArrayList()
    @SerializedName("products")
    var productsEntity: List<ProductsEntity> = ArrayList()
    @SerializedName("cash")
    var cashEntity: CashEntity = CashEntity(0,"", "", "")
}