package com.digio.digioteste.data.products.entities

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class SpotLightEntity (
    @PrimaryKey(autoGenerate = true) val id: Int,

    @SerializedName ("name")
    @ColumnInfo(name = "name")
    val name: String,

    @SerializedName ("bannerURL")
    @ColumnInfo(name = "banner_url")
    val bannerURL: String,

    @SerializedName ("description")
    @ColumnInfo(name = "description")
    val description: String)
