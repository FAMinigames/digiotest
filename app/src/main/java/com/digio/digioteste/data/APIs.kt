package com.digio.digioteste.data

class APIs {
    companion object {
        const val BASE_URL = "https://7hgi9vtkdc.execute-api.sa-east-1.amazonaws.com/"
        const val PRODUCTS_API = "sandbox/products/"
    }
}