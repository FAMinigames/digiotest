package com.digio.digioteste.data.products.api

import com.digio.digioteste.data.ProductsDataResponse
import com.digio.digioteste.data.APIs.Companion.PRODUCTS_API
import retrofit2.http.GET

interface ProductsApi {
    @GET(PRODUCTS_API)
    suspend fun getAllData(): ProductsDataResponse
}