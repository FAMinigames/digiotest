package com.digio.digioteste.feature.product.repository

import android.arch.persistence.room.Room
import android.content.Context
import com.digio.digioteste.feature.product.repository.data.database.ProductsDatabase
import com.digio.digioteste.feature.product.repository.data.database.ProductsDatabase.Companion.PRODUCTS_DATABASE_NAME
import com.digio.digioteste.global.interfaces.IProductRepository
import com.digio.digioteste.global.interfaces.IProductsRepository
import com.digio.digioteste.data.ProductsDataResponse
import com.digio.digioteste.data.products.api.ProductsApi
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.async
import kotlinx.coroutines.withContext
import javax.inject.Inject

@Suppress("UNCHECKED_CAST")
class ProductsRepositoryImpl @Inject constructor(var productsApi: ProductsApi) : IProductsRepository,
    IProductRepository {

    private lateinit var productsDatabase: ProductsDatabase

    override suspend fun <T> getAllProducts(context: Context): T = withContext(IO){
        initializeDatabe(context)

        async {

            var productsDataResponse = ProductsDataResponse()
            val spotLightList = productsDatabase.spotLightDao().getSpotLightList()

            if (spotLightList.isEmpty()) {
                productsDataResponse = productsApi.getAllData()
                productsDatabase.spotLightDao().insertSpotLightList(productsDataResponse.spotLightEntity )
                productsDatabase.productsDao().insertProductsList(productsDataResponse.productsEntity)
                productsDatabase.cashDao().insertCash(productsDataResponse.cashEntity)
            } else {
                productsDataResponse.spotLightEntity = spotLightList
                productsDataResponse.productsEntity = productsDatabase.productsDao().getProductsList()
                productsDataResponse.cashEntity = productsDatabase.cashDao().getCash()
            }

            productsDataResponse as T
        }
    }.await()

    override suspend fun <T> getSpotLight(context: Context, name: String): T = withContext(IO) {
        initializeDatabe(context)

        async {
            productsDatabase.spotLightDao().getSpotLightProduct(name) as T
        }
    }.await()

    override suspend fun <T> getCash(context: Context, name: String): T = withContext(IO) {
        initializeDatabe(context)

        async {
            productsDatabase.cashDao().getCash() as T
        }
    }.await()

    override suspend fun <T> getProducts(context: Context, name: String): T = withContext(IO) {
        initializeDatabe(context)

        async {
            productsDatabase.productsDao().getSpotLightProduct(name) as T
        }
    }.await()

    private fun initializeDatabe(context: Context) {

        productsDatabase = Room.databaseBuilder(
            context,
            ProductsDatabase::class.java,
            PRODUCTS_DATABASE_NAME
        )
            .fallbackToDestructiveMigration()
            .build()
    }
}