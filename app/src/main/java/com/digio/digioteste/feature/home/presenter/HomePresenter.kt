package com.digio.digioteste.feature.home.presenter

import android.view.View
import com.digio.digioteste.R
import com.digio.digioteste.global.controller.AppController.AppResources.appContext
import com.digio.digioteste.global.controller.AppController.AppResources.resources
import com.digio.digioteste.global.utils.AndroidUtils.getUserName

class HomePresenter(val helloUserText: String = resources.getString(R.string.hello_user) + getUserName(),
                    var spotlightProduct: ProductsPresenter,
                    var cashProduct: ProductsPresenter,
                    var productsProduct: ProductsPresenter,
                    val cashTextVisibility: Int = View.VISIBLE)