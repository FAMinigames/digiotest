@file:Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS",
    "RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS"
)

package com.digio.digioteste.feature.productdetails.view

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import com.digio.digioteste.R
import com.digio.digioteste.feature.productdetails.presenter.ProductDetailsPresenter
import com.digio.digioteste.feature.productdetails.viewmodel.DetailsViewModel
import com.digio.digioteste.global.base.BaseActivity
import com.digio.digioteste.global.controller.objects.DaggerController
import com.digio.digioteste.global.factory.ViewModelFactory
import com.digio.digioteste.global.utils.GlideUtils
import com.digio.digioteste.global.utils.ViewUtils.getDp
import kotlinx.android.synthetic.main.activity_details.*
import javax.inject.Inject

class DetailsActivity : BaseActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    lateinit var detailsViewModel: DetailsViewModel
    private var productSelected: String = ""
    private var productName: String = ""

    companion object {
        private const val PRODUCT_SELECTED = "PRODUCT_SELECTED"
        private const val PRODUCT_NAME = "PRODUCT_NAME"

        fun newIntent(context: Context, itemType: String, name: String): Intent {
            val intent = Intent(context, DetailsActivity::class.java)
            intent.putExtra(PRODUCT_SELECTED, itemType)
            intent.putExtra(PRODUCT_NAME, name)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        DaggerController.appComponent.inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun getLayout(): Int = R.layout.activity_details

    override fun initialize() {

        if (intent.extras != null) {
            productSelected = intent.extras.getString(PRODUCT_SELECTED)
            productName = intent.extras.getString(PRODUCT_NAME)
        }

        detailsViewModel = ViewModelProviders.of(this, viewModelFactory).get(DetailsViewModel::class.java)
        detailsViewModel.dataResponse
            .observe(this, Observer<ProductDetailsPresenter>
            {presenter: ProductDetailsPresenter? ->
                run {
                    if (presenter != null) {
                        GlideUtils.loadImage(this, presenter.imageURL, ivBanner)
                        ivBanner.layoutParams =
                            ConstraintLayout.LayoutParams(ivBanner.width, getDp(presenter.heightSize))
                        tvProductName.text = presenter.name
                        tvDescription.text = presenter.description
                    } else {
                        finish()
                    }
                }
            })

        detailsViewModel.getDataAsync(this, productSelected, productName)
    }
}