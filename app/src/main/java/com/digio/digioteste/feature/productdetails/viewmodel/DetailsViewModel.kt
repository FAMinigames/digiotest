package com.digio.digioteste.feature.productdetails.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.content.Context
import com.digio.digioteste.feature.product.repository.ProductsRepositoryImpl
import com.digio.digioteste.feature.productdetails.presenter.ProductDetailsPresenter
import com.digio.digioteste.feature.productdetails.converter.DetailsConverter
import com.digio.digioteste.global.base.BaseViewModelAll
import com.digio.digioteste.global.enums.ProductType
import com.digio.digioteste.global.interfaces.IViewModelStreamData
import com.digio.digioteste.data.products.entities.CashEntity
import com.digio.digioteste.data.products.entities.ProductsEntity
import com.digio.digioteste.data.products.entities.SpotLightEntity
import kotlinx.coroutines.async

class DetailsViewModel(private var detailsConverter: DetailsConverter,
                       private var productsRepositoryImpl: ProductsRepositoryImpl) :
    BaseViewModelAll(), IViewModelStreamData {

    val dataResponse: MutableLiveData<ProductDetailsPresenter> = MutableLiveData()

        override fun getDataAsync(context: Context, businessType: String, name: String) {
            jobs add async {
                try {
                    when (businessType) {
                        ProductType.SPOTLIGHT.name -> {
                            dataResponse.postValue(detailsConverter
                                .convert(productsRepositoryImpl
                                    .getSpotLight(context, name) as SpotLightEntity))
                        }
                        ProductType.CASH.name -> {
                            dataResponse.postValue(detailsConverter
                                .convert(productsRepositoryImpl
                                    .getCash(context, name) as CashEntity))
                        }
                        ProductType.PRODUCT.name -> {
                            dataResponse.postValue(detailsConverter
                                .convert(productsRepositoryImpl
                                    .getProducts(context, name) as ProductsEntity))
                        }
                        else -> {}
                    }
                } catch (t: Throwable) {

                }
            }
        }

}