package com.digio.digioteste.feature.product.repository.data.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import com.digio.digioteste.data.products.entities.ProductsEntity

@Dao
interface ProductsDao {

    @Insert
    fun insertProductsList(spotLightEntityList: List<ProductsEntity>) : List<Long>

    @Query("SELECT * FROM productsEntity")
    fun getProductsList() : List<ProductsEntity>

    @Query("SELECT * FROM productsEntity WHERE name = :name")
    fun getSpotLightProduct(name: String) : ProductsEntity

    @Delete
    fun deleteProductsList(spotLightEntityList: List<ProductsEntity>)
}