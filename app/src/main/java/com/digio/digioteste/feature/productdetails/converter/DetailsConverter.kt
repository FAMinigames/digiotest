package com.digio.digioteste.feature.productdetails.converter

import com.digio.digioteste.feature.productdetails.converter.DetailsConverter.ImageSizes.DIGIOCASH_HEIGHT_SIZE
import com.digio.digioteste.feature.productdetails.converter.DetailsConverter.ImageSizes.PRODUCTS_HEIGHT_SIZE
import com.digio.digioteste.feature.productdetails.converter.DetailsConverter.ImageSizes.SPOTLIGHT_HEIGHT_SIZE
import com.digio.digioteste.feature.productdetails.presenter.ProductDetailsPresenter
import com.digio.digioteste.data.products.entities.CashEntity
import com.digio.digioteste.data.products.entities.ProductsEntity
import com.digio.digioteste.data.products.entities.SpotLightEntity
import javax.inject.Inject

class DetailsConverter @Inject constructor(){

    object ImageSizes {
        const val SPOTLIGHT_HEIGHT_SIZE = 180f
        const val DIGIOCASH_HEIGHT_SIZE = 100f
        const val PRODUCTS_HEIGHT_SIZE = 340f
    }

    fun convert(spotLightEntity: SpotLightEntity) =
        ProductDetailsPresenter(
            name = spotLightEntity.name,
            description = spotLightEntity.description,
            imageURL = spotLightEntity.bannerURL,
            heightSize = SPOTLIGHT_HEIGHT_SIZE
        )

    fun convert(cashEntity: CashEntity) =
        ProductDetailsPresenter(
            name = cashEntity.title,
            description = cashEntity.description,
            imageURL = cashEntity.bannerURL,
            heightSize = DIGIOCASH_HEIGHT_SIZE
        )

    fun convert(productsEntity: ProductsEntity)   =
        ProductDetailsPresenter(
            name = productsEntity.name,
            description = productsEntity.description,
            imageURL = productsEntity.ImageURL,
            heightSize = PRODUCTS_HEIGHT_SIZE
        )
}