package com.digio.digioteste.feature.product.adapter

import android.view.View
import com.digio.digioteste.R
import com.digio.digioteste.feature.product.interfaces.ClickProductListener
import com.digio.digioteste.feature.product.presenter.ProductItemPresenter
import com.digio.digioteste.global.enums.ProductType
import com.digio.digioteste.global.generics.GenericRecyclerView
import com.digio.digioteste.global.generics.GenericViewHolder
import com.digio.digioteste.global.utils.GlideUtils.loadImage
import kotlinx.android.synthetic.main.item_layout.view.*

class ProductsAdapter(productItemPresenterList: List<ProductItemPresenter>, var clickProductItem: ClickProductListener) :
    GenericRecyclerView<ProductItemPresenter, ProductsAdapter.ViewHolder>() {

    override val itemList: List<ProductItemPresenter> = productItemPresenterList

    override fun getLayout() : Int = R.layout.item_layout

    override fun getViewHolder(itemView: View, viewType: Int): ViewHolder =
        ViewHolder(itemView)

    override fun onBindViewHolder(holder: ViewHolder, item: ProductItemPresenter, position: Int) {
        holder.bind(item, clickProductItem)
    }

    class ViewHolder(view: View) : GenericViewHolder(view) {
        fun bind(item: ProductItemPresenter, clickProductItem: ClickProductListener) = with(itemView) {
            itemView.layoutParams = item.layoutParams
            try {
                loadImage(context, item.urlImage, ivProduct)
                if (item.productType == ProductType.PRODUCT) {
                    ivProduct.scaleX = 0.5f
                    ivProduct.scaleY = 0.5f
                }
            } catch (exception: Exception) {

            }
            itemView.setOnClickListener {
                clickProductItem.onClickProductItem(item.productType.name, item.name)
            }
        }
    }

}