package com.digio.digioteste.feature.home.presenter

import android.view.View
import com.digio.digioteste.feature.product.presenter.ProductItemPresenter
import com.digio.digioteste.global.custom.CustomDecorator

class ProductsPresenter(val productItemPresenterList: List<ProductItemPresenter> = ArrayList(),
                        val productName: String = "",
                        val productNameVisibility: Int = View.VISIBLE,
                        val customDecorator: CustomDecorator = CustomDecorator()
)