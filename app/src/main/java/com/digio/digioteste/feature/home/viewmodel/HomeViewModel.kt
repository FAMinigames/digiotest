package com.digio.digioteste.feature.home.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.content.Context
import com.digio.digioteste.feature.home.converter.HomeConverter
import com.digio.digioteste.feature.home.presenter.HomePresenter
import com.digio.digioteste.feature.product.repository.ProductsRepositoryImpl
import com.digio.digioteste.global.base.BaseViewModelAll
import com.digio.digioteste.global.interfaces.IViewModelStreamAllData
import kotlinx.coroutines.async

class HomeViewModel (
    private var homeConverter: HomeConverter,
    private var homeRepositoryImpl: ProductsRepositoryImpl) : BaseViewModelAll(), IViewModelStreamAllData {

    val dataResponse: MutableLiveData<HomePresenter> = MutableLiveData()

    override fun getAllDataAsync(context: Context) {
        jobs add async {
            try {
                val homePresenter = homeConverter.convert(homeRepositoryImpl.getAllProducts(context))
                dataResponse.postValue(homePresenter)
            } catch (t: Throwable) {
                dataResponse.postValue(null)
            }

        }
    }
}