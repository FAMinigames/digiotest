package com.digio.digioteste.feature.product.view

import android.content.Context
import android.graphics.Color
import android.support.v7.widget.LinearLayoutManager
import android.text.Html
import android.util.AttributeSet
import android.view.View
import com.digio.digioteste.R
import com.digio.digioteste.feature.home.presenter.ProductsPresenter
import com.digio.digioteste.feature.product.adapter.ProductsAdapter
import com.digio.digioteste.feature.product.interfaces.ClickProductListener
import com.digio.digioteste.global.base.BaseComponent
import kotlinx.android.synthetic.main.layout_products_component.view.*

@Suppress("DEPRECATION")
class ProductsView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : BaseComponent(context, attrs, defStyleAttr), ClickProductListener {

    private lateinit var clickProductItem: ClickProductListener

    override fun getLayout(): Int  = R.layout.layout_products_component

    override fun initialize(view: View) {

    }

    fun setPresenter(presenter: ProductsPresenter, clickProductItem: ClickProductListener) {
        this.clickProductItem = clickProductItem
        tvBusinessProduct.text = Html.fromHtml(presenter.productName)
        tvBusinessProduct.visibility = presenter.productNameVisibility
        rvBusinessProduct.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        rvBusinessProduct.setBackgroundColor(Color.TRANSPARENT)
        rvBusinessProduct.addItemDecoration(presenter.customDecorator)
        rvBusinessProduct.rvBusinessProduct.adapter =
            ProductsAdapter(presenter.productItemPresenterList, clickProductItem)
    }

    override fun onClickProductItem(productType: String, name: String) {
        clickProductItem.onClickProductItem(productType, name)
    }
}