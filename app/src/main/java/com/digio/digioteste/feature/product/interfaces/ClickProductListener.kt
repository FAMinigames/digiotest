package com.digio.digioteste.feature.product.interfaces

interface ClickProductListener {
    fun onClickProductItem(productType: String, name: String)
}