package com.digio.digioteste.feature.productdetails.presenter

class ProductDetailsPresenter(val name: String, val description: String, val imageURL: String, val heightSize: Float)