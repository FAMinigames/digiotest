package com.digio.digioteste.feature.product.repository.data.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import com.digio.digioteste.data.products.entities.SpotLightEntity

@Dao
interface SpotLightDao {

    @Insert
    fun insertSpotLightList(spotLightEntityList: List<SpotLightEntity>) : List<Long>

    @Query("SELECT * FROM spotLightEntity")
    fun getSpotLightList() : List<SpotLightEntity>

    @Query("SELECT * FROM spotLightEntity WHERE name = :name")
    fun getSpotLightProduct(name: String) : SpotLightEntity

    @Delete
    fun deleteSpotLightList(spotLightEntityList: List<SpotLightEntity>)
}