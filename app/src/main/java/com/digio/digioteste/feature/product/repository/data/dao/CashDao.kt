package com.digio.digioteste.feature.product.repository.data.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import com.digio.digioteste.data.products.entities.CashEntity

@Dao
interface CashDao {

    @Insert
    fun insertCash(cashEntity: CashEntity) : Long

    @Query("SELECT * FROM cashEntity")
    fun getCash() : CashEntity

    @Delete
    fun deleteCashList(cashEntity: CashEntity)
}