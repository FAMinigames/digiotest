package com.digio.digioteste.feature.product.repository.data.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.digio.digioteste.BuildConfig
import com.digio.digioteste.feature.product.repository.data.dao.CashDao
import com.digio.digioteste.feature.product.repository.data.dao.ProductsDao
import com.digio.digioteste.feature.product.repository.data.dao.SpotLightDao
import com.digio.digioteste.data.products.entities.CashEntity
import com.digio.digioteste.data.products.entities.ProductsEntity
import com.digio.digioteste.data.products.entities.SpotLightEntity

@Database(
    entities = [SpotLightEntity::class, ProductsEntity::class, CashEntity::class],
    version = BuildConfig.VERSION_CODE,
    exportSchema = false
)
abstract class ProductsDatabase : RoomDatabase() {

    abstract fun spotLightDao() : SpotLightDao
    abstract fun productsDao() : ProductsDao
    abstract fun cashDao() : CashDao

    companion object {
        const val PRODUCTS_DATABASE_NAME = "db_home"
    }
}