package com.digio.digioteste.feature.product.presenter

import android.widget.FrameLayout
import com.digio.digioteste.global.enums.ProductType

class ProductItemPresenter(val productType: ProductType,
                           val name: String,
                           var urlImage: String = "",
                           val description: String,
                           val layoutParams: FrameLayout.LayoutParams
)