package com.digio.digioteste.feature.product.factory

import android.widget.FrameLayout
import com.digio.digioteste.global.enums.ProductType
import com.digio.digioteste.feature.product.presenter.ProductItemPresenter
import com.digio.digioteste.global.utils.ViewUtils.getDp
import com.digio.digioteste.data.ProductsDataResponse


object ProductsFactory {
    fun getProductList(productsDataResponse: ProductsDataResponse
                       , productType: ProductType
    ) : MutableList<ProductItemPresenter> {
        when (productType) {
            ProductType.SPOTLIGHT -> {
                val spotLightList:MutableList<ProductItemPresenter> = ArrayList()
                for (spotLightItem in productsDataResponse.spotLightEntity) {
                    val spotLight = ProductItemPresenter(
                        ProductType.SPOTLIGHT,
                        spotLightItem.name,
                        spotLightItem.bannerURL,
                        spotLightItem.description,
                        FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, getDp(160f))
                    )
                    spotLightList.add(spotLight)
                }
                return spotLightList
            }
            ProductType.CASH -> {
                val cashList:MutableList<ProductItemPresenter> = ArrayList()
                val cashItem = ProductItemPresenter(
                    ProductType.CASH,
                    productsDataResponse.cashEntity.title,
                    productsDataResponse.cashEntity.bannerURL,
                    productsDataResponse.cashEntity.description,
                    FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, getDp(95f))
                )
                cashList.add(cashItem)
                return cashList
            }
            ProductType.PRODUCT -> {
                val productsList:MutableList<ProductItemPresenter> = ArrayList()
                for (productItem in productsDataResponse.productsEntity) {
                    val product = ProductItemPresenter(
                        ProductType.PRODUCT,
                        productItem.name,
                        productItem.ImageURL,
                        productItem.description,
                        FrameLayout.LayoutParams(getDp(115f), getDp(115f))
                    )
                    productsList.add(product)
                }

                return productsList
            }
        }



    }
}