package com.digio.digioteste.feature.home.navigator

import android.app.ActivityOptions
import android.content.Context
import android.os.Build
import android.support.annotation.RequiresApi
import com.digio.digioteste.feature.home.view.HomeActivity
import com.digio.digioteste.feature.productdetails.view.DetailsActivity
import javax.inject.Inject

class HomeNavigator @Inject constructor() {

    lateinit var mHomeActivity: HomeActivity

    internal fun initialize(homeActivity: HomeActivity) {
        mHomeActivity = homeActivity
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    internal fun onClickItem(context: Context, itemType: String, name: String) {
        context.startActivity(DetailsActivity.newIntent(context, itemType, name),
            ActivityOptions.makeSceneTransitionAnimation(mHomeActivity).toBundle())
    }
}