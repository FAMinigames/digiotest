package com.digio.digioteste.feature.home.view

import android.app.AlertDialog
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.view.View.GONE
import com.digio.digioteste.R
import com.digio.digioteste.feature.home.navigator.HomeNavigator
import com.digio.digioteste.feature.home.presenter.HomePresenter
import com.digio.digioteste.feature.home.viewmodel.HomeViewModel
import com.digio.digioteste.feature.product.interfaces.ClickProductListener
import com.digio.digioteste.global.base.BaseActivity
import com.digio.digioteste.global.controller.objects.DaggerController
import com.digio.digioteste.global.factory.ViewModelFactory
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.layout_hello_user.*
import javax.inject.Inject

class HomeActivity : BaseActivity(), ClickProductListener {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    lateinit var homeViewModel: HomeViewModel

    @Inject
    lateinit var homeNavigator: HomeNavigator

    override fun onCreate(savedInstanceState: Bundle?) {
        DaggerController.appComponent.inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun getLayout(): Int = R.layout.activity_main

    override fun initialize() {
        homeNavigator.initialize(this)
        homeViewModel = ViewModelProviders.of(this, viewModelFactory).get(HomeViewModel::class.java)
        homeViewModel.dataResponse
            .observe(this,  Observer<HomePresenter>
        { homePresenter: HomePresenter? ->
            if (homePresenter != null) {
                tvHelloUser.text = homePresenter.helloUserText
                pcSpotLight.setPresenter(homePresenter.spotlightProduct, this)
                pcDigioCash.setPresenter(homePresenter.cashProduct, this)
                pcProducts.setPresenter(homePresenter.productsProduct, this)
                shimmerParent.visibility = GONE
            } else {
                openAlertDialog()
            }
        })

        homeViewModel.getAllDataAsync(this)
    }

    private fun openAlertDialog() {
        val alertDialog = AlertDialog.Builder(this).create()
        alertDialog.setMessage(getString(R.string.failed_to_fetch_dataa))
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.close))
        { dialog, _ ->
            dialog.dismiss()
            finish()
        }
        alertDialog.show()
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onClickProductItem(productType: String, name: String) {
        homeNavigator.onClickItem(this, productType, name)
    }

}
