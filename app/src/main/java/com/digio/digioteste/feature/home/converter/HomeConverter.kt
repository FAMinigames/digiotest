package com.digio.digioteste.feature.home.converter

import android.view.View.GONE
import com.digio.digioteste.R
import com.digio.digioteste.global.enums.ProductType
import com.digio.digioteste.feature.home.presenter.HomePresenter
import com.digio.digioteste.feature.home.presenter.ProductsPresenter
import com.digio.digioteste.feature.product.factory.ProductsFactory.getProductList
import com.digio.digioteste.global.controller.AppController.AppResources.resources
import com.digio.digioteste.global.custom.CustomDecorator
import com.digio.digioteste.data.ProductsDataResponse
import javax.inject.Inject

class HomeConverter @Inject constructor(){

    fun convert(productsDataResponse: ProductsDataResponse) : HomePresenter {

        return HomePresenter(
            spotlightProduct = ProductsPresenter(
                getProductList(productsDataResponse, ProductType.SPOTLIGHT),
                productName = resources.getString(R.string.products), productNameVisibility = GONE
            ),

            cashProduct = ProductsPresenter(
                getProductList(productsDataResponse, ProductType.CASH),
                productName = resources.getString(R.string.digio_cash)
            ),

            productsProduct = ProductsPresenter(
                getProductList(productsDataResponse, ProductType.PRODUCT),
                productName = resources.getString(R.string.products), customDecorator = CustomDecorator
                    .Builder()
                    .setRecycleViewWidthSize(200)
                    .setHorizontalSpacing(100)
                    .build()
            )
        )
    }

}