package com.digio.digioteste.di.component


import com.digio.digioteste.di.modules.ViewModelModule
import com.digio.digioteste.di.modules.ProductsApiModule
import com.digio.digioteste.feature.home.view.HomeActivity
import com.digio.digioteste.feature.productdetails.view.DetailsActivity
import dagger.Component

@Component(modules = [ProductsApiModule::class, ViewModelModule::class])
interface AppComponent {
    fun inject(homeActivity: HomeActivity)
    fun inject(detailsActivity: DetailsActivity)
}