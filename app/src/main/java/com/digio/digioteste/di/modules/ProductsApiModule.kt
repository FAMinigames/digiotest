package com.digio.digioteste.di.modules

import com.digio.digioteste.data.products.api.ProductsApi
import com.digio.digioteste.global.controller.objects.RetrofitController.retrofit
import dagger.Module
import dagger.Provides


@Module
class ProductsApiModule {

    @Provides
    fun providesProductApi() : ProductsApi {
        return retrofit!!.create(ProductsApi::class.java)
    }

}