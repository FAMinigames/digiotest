package com.digio.digioteste.di.modules

import android.arch.lifecycle.ViewModel
import dagger.multibindings.IntoMap
import dagger.Binds
import com.digio.digioteste.global.factory.ViewModelFactory
import android.arch.lifecycle.ViewModelProvider
import com.digio.digioteste.feature.home.viewmodel.HomeViewModel
import com.digio.digioteste.feature.productdetails.viewmodel.DetailsViewModel
import com.digio.digioteste.global.interfaces.ViewModelKey
import dagger.Module


@Module
abstract class ViewModelModule {

    @Binds
    abstract fun bindViewModelFactory(viewModelFactory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(HomeViewModel::class)
    abstract fun providesHomeViewModel(homeViewModel: HomeViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DetailsViewModel::class)
    abstract fun providesDetailsViewModel(detailsViewModel: DetailsViewModel): ViewModel
}