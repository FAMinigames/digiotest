package com.digio.digioteste.global.utils

import com.digio.digioteste.global.controller.AppController


object AndroidUtils {

    fun getUserName() : String {
        val manager = AppController.AppResources.accountManager
        val list = manager!!.accounts
        if (list != null && list.isNotEmpty()) {
            return formatName(list[0].name)
        } else {
            return "Usuario"
        }
    }

    private fun formatName(name: String) : String {
        var found = false
        var finalName = ""

        for (letter: Char in name.toCharArray()) {
            if (letter == "@".single()) {
                found = true
            } else {
                if (!found) {
                    finalName += letter.toString()
                }
            }
        }

        return " " + finalName.capitalize()
    }

}