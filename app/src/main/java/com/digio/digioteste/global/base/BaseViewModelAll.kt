package com.digio.digioteste.global.base

import android.arch.lifecycle.ViewModel
import com.digio.digioteste.global.interfaces.IViewModelStreamAllData
import com.digio.digioteste.global.interfaces.IViewModelStreamData
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.Job

abstract class BaseViewModelAll: ViewModel(), CoroutineScope {

    override val coroutineContext = Main
    protected val jobs = ArrayList<Job>()

    infix fun ArrayList<Job>.add(job: Job) {
        this.add(job)
    }

    override fun onCleared() {
        super.onCleared()
        jobs.forEach { if(!it.isCancelled) it.cancel() }
    }
}