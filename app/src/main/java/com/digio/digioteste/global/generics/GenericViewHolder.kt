package com.digio.digioteste.global.generics

import android.support.v7.widget.RecyclerView
import android.view.View

open class GenericViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)