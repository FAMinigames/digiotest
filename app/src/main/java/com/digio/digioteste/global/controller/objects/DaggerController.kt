package com.digio.digioteste.global.controller.objects

import com.digio.digioteste.di.component.AppComponent
import com.digio.digioteste.di.component.DaggerAppComponent

object DaggerController {
    var appComponent : AppComponent = DaggerAppComponent
        .builder()
        .build()
}