package com.digio.digioteste.global.utils

import com.digio.digioteste.global.controller.AppController

object ViewUtils {
    fun getDp(dp: Float): Int {
        return (dp * AppController.AppResources.resources.displayMetrics.density).toInt()
    }
}