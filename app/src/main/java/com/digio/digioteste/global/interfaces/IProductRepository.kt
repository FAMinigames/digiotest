package com.digio.digioteste.global.interfaces

import android.content.Context

interface IProductRepository {
    suspend fun <T> getSpotLight(context: Context, name: String): T
    suspend fun <T> getCash(context: Context, name: String): T
    suspend fun <T> getProducts(context: Context, name: String): T
}