package com.digio.digioteste.global.interfaces

import android.support.annotation.LayoutRes

interface ILayout {
    @LayoutRes
    fun getLayout() : Int
}