package com.digio.digioteste.global.base

import android.content.Context
import android.os.Build
import android.support.v7.widget.CardView
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import com.digio.digioteste.global.interfaces.ILayout

abstract class BaseComponent @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) :
    FrameLayout(context, attrs, defStyleAttr), ILayout  {

    init {
        inflate()
    }

    private fun inflate() {
        val view = View.inflate(context, getLayout(), this)

        initialize(view)
    }

    abstract fun initialize(view: View)


}