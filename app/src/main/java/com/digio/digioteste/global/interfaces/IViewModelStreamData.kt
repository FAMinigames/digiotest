package com.digio.digioteste.global.interfaces

import android.content.Context

interface IViewModelStreamData {
    fun getDataAsync(context: Context, businessType: String, name: String)
}