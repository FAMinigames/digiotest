package com.digio.digioteste.global.interfaces

import android.content.Context

interface IViewModelStreamAllData {
    fun getAllDataAsync(context: Context)
}