package com.digio.digioteste.global.enums

enum class ProductType {
    SPOTLIGHT,
    PRODUCT,
    CASH,
}