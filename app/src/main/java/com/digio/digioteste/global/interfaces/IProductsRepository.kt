package com.digio.digioteste.global.interfaces

import android.content.Context

interface  IProductsRepository {
    suspend fun <T> getAllProducts(context: Context): T
}