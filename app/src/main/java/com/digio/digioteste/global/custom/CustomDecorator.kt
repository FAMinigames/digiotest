package com.digio.digioteste.global.custom

import android.graphics.Rect
import android.support.v7.widget.RecyclerView
import android.view.View

class CustomDecorator : RecyclerView.ItemDecoration() {

    private var recycleViewWidthSize: Int = 0
    private var mHorizontalSpacing: Int = 50
    private var mHasSpaceOnHorizontalSides: Boolean = true
    private val NO_SPACE = 0

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        super.getItemOffsets(outRect, view, parent, state)

        val position = parent.getChildViewHolder(view).adapterPosition
        val itemCount = state.itemCount
        setSpacingForDirection(outRect, position, itemCount)
    }

    private fun setSpacingForDirection(outRect: Rect, position: Int, itemCount: Int) {

        val FIRST_ITEM = 0
        val LAST_ITEM = itemCount - 1

        if (mHasSpaceOnHorizontalSides) {
            outRect.left = mHorizontalSpacing
            outRect.right = if (position == LAST_ITEM) mHorizontalSpacing else NO_SPACE
        } else {
            outRect.left = if (position == FIRST_ITEM) NO_SPACE else mHorizontalSpacing
            outRect.right = if (position == LAST_ITEM) NO_SPACE else mHorizontalSpacing
        }

        outRect.top = NO_SPACE
        outRect.bottom = NO_SPACE
    }

    fun getRecycleViewWidthSize(): Int {
        return recycleViewWidthSize
    }

    class Builder {

        private val mInstance: CustomDecorator =
            CustomDecorator()

        fun setRecycleViewWidthSize(recycleViewWidthSize: Int): Builder {
            mInstance.recycleViewWidthSize = recycleViewWidthSize
            return this
        }

        fun setHorizontalSpacing(horizontalSpacing: Int): Builder {
            mInstance.mHorizontalSpacing = horizontalSpacing
            return this
        }

        fun setHasSpaceOnHorizontalSides(hasSpaceOnHorizontalSides: Boolean): Builder {
            mInstance.mHasSpaceOnHorizontalSides = hasSpaceOnHorizontalSides
            return this
        }

        fun build(): CustomDecorator {
            return mInstance
        }

    }

}