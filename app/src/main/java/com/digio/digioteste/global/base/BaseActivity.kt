package com.digio.digioteste.global.base

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.digio.digioteste.global.interfaces.ILayout
import dagger.android.AndroidInjection
import dagger.android.HasActivityInjector

abstract class BaseActivity : AppCompatActivity(), ILayout/*, HasActivityInjector*/ {

    override fun onCreate(savedInstanceState: Bundle?) {
        //AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(getLayout())
        initialize()
    }

    protected abstract fun initialize()

}