package com.digio.digioteste.global.factory

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.digio.digioteste.feature.home.converter.HomeConverter
import com.digio.digioteste.feature.product.repository.ProductsRepositoryImpl
import com.digio.digioteste.feature.home.viewmodel.HomeViewModel
import com.digio.digioteste.feature.productdetails.converter.DetailsConverter
import com.digio.digioteste.feature.productdetails.viewmodel.DetailsViewModel
import javax.inject.Inject

@Suppress("UNCHECKED_CAST")
class ViewModelFactory @Inject constructor(
    var homeConverter: HomeConverter,
    var detailsConverter: DetailsConverter,
    var productsRepositoryImpl: ProductsRepositoryImpl): ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return when {
            modelClass.isAssignableFrom(HomeViewModel::class.java) ->
                HomeViewModel(homeConverter, productsRepositoryImpl) as T
            modelClass.isAssignableFrom(DetailsViewModel::class.java) ->
                DetailsViewModel(detailsConverter, productsRepositoryImpl) as T
            else -> IllegalArgumentException("ViewModel Not Found") as T
        }
    }
}