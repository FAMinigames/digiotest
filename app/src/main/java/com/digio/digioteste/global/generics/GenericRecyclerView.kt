package com.digio.digioteste.global.generics

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.digio.digioteste.global.interfaces.ILayout

abstract class GenericRecyclerView<T, VH : GenericViewHolder> : RecyclerView.Adapter<VH>(), ILayout {

    protected lateinit var context: Context

    abstract val itemList: List<T>

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        context = parent.context
        val itemView = LayoutInflater.from(parent.context).inflate(getLayout(), parent, false)
        return getViewHolder(itemView, viewType)
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        onBindViewHolder(holder, itemList[position], position)
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    abstract fun getViewHolder(itemView: View, viewType: Int): VH

    abstract fun onBindViewHolder(holder: VH, item: T, position: Int)
}