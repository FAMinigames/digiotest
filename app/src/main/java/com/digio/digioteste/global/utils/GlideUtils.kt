package com.digio.digioteste.global.utils

import android.content.Context
import android.widget.ImageView
import com.digio.digioteste.R
import com.digio.digioteste.global.controller.GlideApp

object GlideUtils {

    fun loadImage(context: Context, url: String, imageView: ImageView) {
        GlideApp.with(context)
            .load(url)
            .placeholder(R.drawable.no_image)
            .dontAnimate()
            .into(imageView)
    }
}