package com.digio.digioteste.global.controller.base

import android.app.Application
import com.digio.digioteste.global.controller.objects.RetrofitController

abstract class BaseAppController : Application() {

    override fun onCreate() {
        super.onCreate()

        RetrofitController.initializeRetrofit()

        initialize()
    }

    abstract fun initialize()
}