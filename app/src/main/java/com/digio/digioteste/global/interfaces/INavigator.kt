package com.digio.digioteste.global.interfaces

import android.support.v7.app.AppCompatActivity

interface INavigator {
    fun initialize(activity: AppCompatActivity)
}