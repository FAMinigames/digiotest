package com.digio.digioteste.global.controller

import android.accounts.AccountManager
import android.annotation.SuppressLint
import android.content.Context
import android.content.res.Resources
import com.digio.digioteste.global.controller.base.BaseAppController

class AppController : BaseAppController() {

   override fun initialize() {
      AppResources.resources = applicationContext.resources
      AppResources.appContext = applicationContext
      AppResources.accountManager = AppResources.appContext.getSystemService(ACCOUNT_SERVICE) as AccountManager

   }

   @SuppressLint("StaticFieldLeak")
   object AppResources {
      lateinit var accountManager: AccountManager
      lateinit var appContext: Context
      lateinit var resources: Resources
   }

}