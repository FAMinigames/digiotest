package com.digio.digioteste.feature.home.converter

import android.accounts.AccountManager
import android.content.Context
import android.content.res.Resources
import android.util.DisplayMetrics
import com.digio.digioteste.R
import com.digio.digioteste.data.ProductsDataResponse
import com.digio.digioteste.data.products.entities.CashEntity
import com.digio.digioteste.feature.home.presenter.HomePresenter
import com.digio.digioteste.global.controller.AppController
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.mock
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class HomeConverterTest {

    @Mock
    lateinit var context: Context

    @Mock
    lateinit var resources: Resources

    @Mock
    lateinit var accountManager: AccountManager

    @Mock
    lateinit var displayMetrics: DisplayMetrics

    @Mock
    lateinit var productsDataResponse: ProductsDataResponse

    @Before
    fun before() {
        MockitoAnnotations.initMocks(this)
        AppController.AppResources.appContext = context
        AppController.AppResources.resources = resources
        AppController.AppResources.accountManager = accountManager
        Mockito.`when`(resources.getString(R.string.products)).thenReturn("Produtos")
        Mockito.`when`(resources.getString(R.string.digio_cash)).thenReturn("digio Cash")
        Mockito.`when`(productsDataResponse.cashEntity).thenReturn(CashEntity(0, "digio Cash", "banner", "descrição"))
        Mockito.`when`(AppController.AppResources.resources.displayMetrics).thenReturn(displayMetrics)
    }

    @Test
    fun convertTest() {
        // Given
        var homeConverter = HomeConverter()
        var value = "digio Cash"

        // When
        var productsDataResponse = homeConverter.convert(productsDataResponse)

        // Then
        assertEquals(productsDataResponse.cashProduct.productName, value)
    }

}